Forwarded: part of PR#9000 by Jojo-Schmitz
Origin: https://github.com/musescore/MuseScore/pull/7443
Author: worldwideweary <worldwideweary@aol.com>
Description: Fix #317084: SVG honors z-order of staff-lines and includes Staff Type Changes

--- a/mscore/file.cpp
+++ b/mscore/file.cpp
@@ -2875,85 +2875,91 @@ bool MuseScore::saveSvg(Score* score, QI
       if (trimMargin >= 0 && score->npages() == 1)
             p.translate(-r.topLeft());
       MScore::pixelRatio = DPI / printer.logicalDpiX();
-      // 1st pass: StaffLines
-      for  (System* s : page->systems()) {
-            for (int i = 0, n = s->staves()->size(); i < n; i++) {
-                  if (score->staff(i)->invisible() || !score->staff(i)->show())
-                        continue;  // ignore invisible staves
-                  if (s->staves()->isEmpty() || !s->staff(i)->show())
-                        continue;
-                  Measure* fm = s->firstMeasure();
-                  if (!fm) // only boxes, hence no staff lines
-                        continue;
 
-                  // The goal here is to draw SVG staff lines more efficiently.
-                  // MuseScore draws staff lines by measure, but for SVG they can
-                  // generally be drawn once for each system. This makes a big
-                  // difference for scores that scroll horizontally on a single
-                  // page. But there are exceptions to this rule:
-                  //
-                  //   ~ One (or more) invisible measure(s) in a system/staff ~
-                  //   ~ One (or more) elements of type HBOX or VBOX          ~
-                  //
-                  // In these cases the SVG staff lines for the system/staff
-                  // are drawn by measure.
-                  //
-                  bool byMeasure = false;
-                  for (MeasureBase* mb = fm; mb; mb = s->nextMeasure(mb)) {
-                        if (!mb->isMeasure() || !toMeasure(mb)->visible(i)) {
-                              byMeasure = true;
-                              break;
-                              }
-                        }
-                  if (byMeasure) { // Draw visible staff lines by measure
-                        for (MeasureBase* mb = fm; mb; mb = s->nextMeasure(mb)) {
-                              if (mb->isMeasure() && toMeasure(mb)->visible(i)) {
-                                    StaffLines* sl = toMeasure(mb)->staffLines(i);
-                                    printer.setElement(sl);
-                                    paintElement(p, sl);
-                                    }
-                              }
-                        }
-                  else { // Draw staff lines once per system
-                        StaffLines* firstSL = s->firstMeasure()->staffLines(i)->clone();
-                        StaffLines*  lastSL =  s->lastMeasure()->staffLines(i);
-
-                        qreal lastX =  lastSL->bbox().right()
-                                    +  lastSL->pagePos().x()
-                                    - firstSL->pagePos().x();
-                        QVector<QLineF>& lines = firstSL->getLines();
-                        for (int l = 0, c = lines.size(); l < c; l++)
-                              lines[l].setP2(QPointF(lastX, lines[l].p2().y()));
-
-                        printer.setElement(firstSL);
-                        paintElement(p, firstSL);
-                        }
-                  }
-            }
-      // 2nd pass: the rest of the elements
       QList<Element*> pel = page->elements();
-      qStableSort(pel.begin(), pel.end(), elementLessThan);
-      ElementType eType;
+      std::stable_sort(pel.begin(), pel.end(), elementLessThan);
+
+      System* currentSystem               { nullptr };
+      Measure* firstMeasureOfSystem       { nullptr };
+      const Measure* currentMeasure       { nullptr };
+      std::vector<System*> printedSystems;
       for (const Element* e : pel) {
             // Always exclude invisible elements
             if (!e->visible())
                   continue;
+            if (e->type() == ElementType::STAFF_LINES) {
+                  currentMeasure = e->findMeasure();
+                  currentSystem = currentMeasure->system();
 
-            eType = e->type();
-            switch (eType) { // In future sub-type code, this switch() grows, and eType gets used
-            case ElementType::STAFF_LINES : // Handled in the 1st pass above
-                  continue; // Exclude from 2nd pass
-                  break;
-            default:
-                  break;
-            } // switch(eType)
+                  if (std::find(printedSystems.begin(), printedSystems.end(), currentSystem) != printedSystems.end())
+                        continue; // Skip lines if current system has been drawn already
 
+                  firstMeasureOfSystem = currentSystem->firstMeasure();
+                  if (!firstMeasureOfSystem) // only boxes, hence no staff lines
+                        continue;
+                  for (int i = 0, n = currentSystem->staves()->size(); i < n; i++) {
+                        if (score->staff(i)->invisible() || !score->staff(i)->show())
+                              continue;  // ignore invisible staves
+                        if (currentSystem->staves()->isEmpty() || !currentSystem->staff(i)->show())
+                              continue;
+
+                        // Draw SVG lines per entire system for efficiency
+                        // Exceptions (draw SVG staff lines by measure instead):
+                        //
+                        //   One (or more) invisible measure(s) in a system/staff
+                        //   One (or more) elements of type HBOX or VBOX
+                        //   One (or more) Staff Type Change(s) within a system
+                        //
+                        bool byMeasure = false;
+                        for (MeasureBase* mb = firstMeasureOfSystem;
+                             mb && !byMeasure;
+                             mb = currentSystem->nextMeasure(mb)) {
+                              if (!mb->isMeasure() || !toMeasure(mb)->visible(i)) {
+                                    byMeasure = true;
+                                    break;
+                                    }
+                              for (Element* element : toMeasure(mb)->el()) {
+                                    if (element->isStaffTypeChange()) {
+                                          byMeasure = true;
+                                          break;
+                                          }
+                                    }
+                              }
+                        if (byMeasure) {
+                              // Draw visible staff lines by measure (all of current system)
+                              for (MeasureBase* mb = firstMeasureOfSystem; mb; mb = currentSystem->nextMeasure(mb)) {
+                                    if (mb->isMeasure() && toMeasure(mb)->visible(i)) {
+                                          StaffLines* sl = toMeasure(mb)->staffLines(i);
+                                          printer.setElement(sl);
+                                          paintElement(p, sl);
+                                          }
+                                    }
+                              }
+                        else { // Draw staff lines once per system
+                              StaffLines* firstSL = firstMeasureOfSystem->staffLines(i)->clone();
+                              StaffLines*  lastSL = currentSystem->lastMeasure()->staffLines(i);
+
+                              qreal lastX = lastSL->bbox().right()
+                                            + lastSL->pagePos().x()
+                                            - firstSL->pagePos().x();
+                              QVector<QLineF>& lines = firstSL->getLines();
+                              for (int l = 0, c = lines.size(); l < c; l++)
+                                    lines[l].setP2(QPointF(lastX, lines[l].p2().y()));
+
+                              printer.setElement(firstSL);
+                              paintElement(p, firstSL);
+                              }
+                        }
+                  printedSystems.push_back(currentSystem);
+                  continue; // Drawing of staff-line element complete
+                  }
             // Set the Element pointer inside SvgGenerator/SvgPaintEngine
             printer.setElement(e);
 
             // Paint it
             paintElement(p, e);
-            }
+            } // End of element loop
+
       p.end(); // Writes MuseScore SVG file to disk, finally
 
       // Clean up and return
