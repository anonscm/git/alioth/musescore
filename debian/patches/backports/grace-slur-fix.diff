Origin: upstream, commit:28c1c6591a12d788f741d739ff22ca9647667db6
Author: Matt McClinch <mattmcclinch@gmail.com>
Date:   Thu Mar 11 08:38:36 2021 +0100
Description: Fix #302104: Slur and grace note (acciaccatura) fails
 Resolves: https://musescore.org/en/node/302104
 Backport of #5802
 .
 This commit does the following:
 - Allows a slur to be added from a grace note to its main note if a range selection is used.
 - Changes the behavior of applying the Slur palette element to a list selection of more than two notes so that it is consistent with how the "S" keyboard shortcut already behaves.
 - Does not start edit mode for a newly created slur if the selection contains more than one element.
 - Fixes a mathematical error in ChordRest::isBefore() that can cause a slur to be added with an incorrect span if grace notes are involved.

--- a/libmscore/chordrest.cpp
+++ b/libmscore/chordrest.cpp
@@ -1318,8 +1318,11 @@ bool ChordRest::isBefore(const ChordRest
             bool oGrace      = o->isGrace();
             bool grace       = isGrace();
             // normal note are initialized at graceIndex 0 and graceIndex is 0 based
-            int oGraceIndex  = oGrace ? toChord(o)->graceIndex() +  1 : 0;
-            int graceIndex   = grace ? toChord(this)->graceIndex() + 1 : 0;
+            int oGraceIndex  = toChord(o)->graceIndex();
+            int graceIndex   = toChord(this)->graceIndex();
+            // Smaller indexes are further away from the note, and larger indexes are closer to the note.
+            // We want to reverse that. Subtracting a 0-based index from the size results in a 1-based index,
+            // which is exactly what we want.
             if (oGrace)
                   oGraceIndex = toChord(o->parent())->graceNotes().size() - oGraceIndex;
             if (grace)
--- a/mscore/palette.cpp
+++ b/mscore/palette.cpp
@@ -522,7 +522,7 @@ void Palette::applyPaletteElement(Palett
                   LayoutBreak* breakElement = toLayoutBreak(element);
                   score->cmdToggleLayoutBreak(breakElement->layoutBreakType());
                   }
-            else if (element->isSlur() && addSingle) {
+            else if (element->isSlur()) {
                   viewer->addSlur();
                   }
             else if (element->isSLine() && !element->isGlissando() && addSingle) {
--- a/mscore/scoreview.cpp
+++ b/mscore/scoreview.cpp
@@ -3362,9 +3362,9 @@ void ScoreView::addSlur()
                         if (!e->isChord())
                               continue;
                         ChordRest* cr = toChordRest(e);
-                        if (!cr1 || cr1->tick() > cr->tick())
+                        if (!cr1 || cr->isBefore(cr1))
                               cr1 = cr;
-                        if (!cr2 || cr2->tick() < cr->tick())
+                        if (!cr2 || cr2->isBefore(cr))
                               cr2 = cr;
                         }
                   if (cr1 && (cr1 != cr2))
@@ -3432,7 +3432,7 @@ void ScoreView::cmdAddSlur(ChordRest* cr
             _score->inputState().setSlur(slur);
             ss->setSelected(true);
             }
-      else if (startEditMode) {
+      else if (startEditMode && _score()->selection().isSingle()) {
             editData.element = ss;
             changeState(ViewState::EDIT);
             }
