Origin: upstream, commit:7ab33e06b2b8446a433f4fb974ef71541d352224
Author: Dmitri Ovodok <dmitrio95@yandex.ru>
Description: fix #301561: Plugin API: restore pagenumber and pagePos properties from MuseScore 2.X

--- a/mscore/plugin/api/elements.cpp
+++ b/mscore/plugin/api/elements.cpp
@@ -206,6 +206,15 @@ void Chord::addInternal(Ms::Chord* chord
       }
 
 //---------------------------------------------------------
+//   Page::pagenumber
+//---------------------------------------------------------
+
+int Page::pagenumber() const
+      {
+      return page()->no();
+      }
+
+//---------------------------------------------------------
 //   Chord::remove
 //---------------------------------------------------------
 
@@ -242,6 +251,8 @@ Element* wrap(Ms::Element* e, Ownership
                   return wrap<Segment>(toSegment(e), own);
             case ElementType::MEASURE:
                   return wrap<Measure>(toMeasure(e), own);
+            case ElementType::PAGE:
+                  return wrap<Page>(toPage(e), own);
             default:
                   break;
             }
--- a/mscore/plugin/api/elements.h
+++ b/mscore/plugin/api/elements.h
@@ -20,6 +20,7 @@
 #include "libmscore/measure.h"
 #include "libmscore/note.h"
 #include "libmscore/notedot.h"
+#include "libmscore/page.h"
 #include "libmscore/segment.h"
 #include "libmscore/accidental.h"
 #include "libmscore/types.h"
@@ -108,6 +109,11 @@ class Element : public Ms::PluginAPI::Sc
        * \since MuseScore 3.3
        */
       Q_PROPERTY(qreal posY READ posY)
+      /**
+       * Position of this element in page coordinates, in spatium units.
+       * \since MuseScore 3.5
+       */
+      Q_PROPERTY(QPointF pagePos READ pagePos)
 
       /**
        * Bounding box of this element.
@@ -357,6 +363,8 @@ class Element : public Ms::PluginAPI::Sc
       qreal posX() const { return element()->pos().x() / element()->spatium(); }
       qreal posY() const { return element()->pos().y() / element()->spatium(); }
 
+      QPointF pagePos() const { return element()->pagePos() / element()->spatium(); }
+
       Ms::PluginAPI::Element* parent() const { return wrap(element()->parent()); }
 
       QRectF bbox() const;
@@ -626,6 +634,32 @@ class Measure : public Element {
       /// \endcond
       };
 
+//---------------------------------------------------------
+//   Page
+//---------------------------------------------------------
+
+class Page : public Element {
+      Q_OBJECT
+      /**
+       * \brief Page number, counting from 0.
+       * Number of this page in the score counting from 0, i.e.
+       * for the first page its \p pagenumber value will be equal to 0.
+       * \since MuseScore 3.5
+       */
+      Q_PROPERTY(int pagenumber READ pagenumber)
+
+   public:
+      /// \cond MS_INTERNAL
+      Page(Ms::Page* p = nullptr, Ownership own = Ownership::SCORE)
+         : Element(p, own) {}
+
+      Ms::Page* page() { return toPage(e); }
+      const Ms::Page* page() const { return toPage(e); }
+
+      int pagenumber() const;
+      /// \endcond
+      };
+
 #undef API_PROPERTY
 #undef API_PROPERTY_T
 #undef API_PROPERTY_READ_ONLY
