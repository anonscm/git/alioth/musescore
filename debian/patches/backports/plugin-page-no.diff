Origin: upstream, commit:15a30513d107ea9fc0c341a930cc94e104c9df81
Author: Dmitri Ovodok <dmitrio95@yandex.ru>
Description: Plugin API: add Score.pageNumberOffset property

--- a/mscore/plugin/api/elements.h
+++ b/mscore/plugin/api/elements.h
@@ -644,7 +644,13 @@ class Page : public Element {
        * \brief Page number, counting from 0.
        * Number of this page in the score counting from 0, i.e.
        * for the first page its \p pagenumber value will be equal to 0.
+       * User-visible page number can be calculated as
+       * \code
+       * page.pagenumber + 1 + score.pageNumberOffset
+       * \endcode
+       * where \p score is the relevant \ref Score object.
        * \since MuseScore 3.5
+       * \see Score::pageNumberOffset
        */
       Q_PROPERTY(int pagenumber READ pagenumber)
 
--- a/mscore/plugin/api/score.h
+++ b/mscore/plugin/api/score.h
@@ -86,6 +86,15 @@ class Score : public Ms::PluginAPI::Scor
       Q_PROPERTY(QString                        mscoreRevision    READ mscoreRevision)
       /** Current selections for the score. \since MuseScore 3.3 */
       Q_PROPERTY(Ms::PluginAPI::Selection*      selection         READ selection)
+      /**
+       * Page numbering offset. The user-visible number of the given \p page is defined as
+       * \code
+       * page.pagenumber + 1 + score.pageNumberOffset
+       * \endcode
+       * \since MuseScore 3.5
+       * \see Page::pagenumber
+       */
+      Q_PROPERTY(int pageNumberOffset READ pageNumberOffset WRITE setPageNumberOffset)
 
    public:
       /// \cond MS_INTERNAL
@@ -106,6 +115,9 @@ class Score : public Ms::PluginAPI::Scor
       QString title() { return score()->metaTag("workTitle"); }
       Ms::PluginAPI::Selection* selection() { return selectionWrap(&score()->selection()); }
 
+      int pageNumberOffset() const { return score()->pageNumberOffset(); }
+      void setPageNumberOffset(int offset) { score()->undoChangePageNumberOffset(offset); }
+
       /// \endcond
 
       /// Returns as a string the metatag named \p tag
